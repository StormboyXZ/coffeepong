import java.io.IOException;
import java.nio.file.Paths;
import java.util.Random;
import org.jsfml.graphics.*;
import org.jsfml.system.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;
import org.jsfml.audio.*;

public class CoffeePong
{
    //Game components
    private static RenderWindow window;
    private static Texture spriteSheet, ballTex;
    private static Sprite playerPaddle, cpuPaddle;
    private static Sprite ball;
    private static Sprite upperBound, lowerBound;
    private static Random rand;
    private static RectangleShape mid;
    private static Font font;
    private static SoundBuffer hitBuff, menuBuff, winBuff;
    private static Sound hitSound, menuSound, winSound;
    private static final int WIDTH = 800;
    private static final int HEIGHT = 600;
    
    public static void main(String[] args)
    {
        //Create the window
        window = new RenderWindow(new VideoMode(WIDTH,HEIGHT,32), "CoffeePong");
        window.setVerticalSyncEnabled(true);
        
        //Load the textures
        spriteSheet = new Texture();
        ballTex = new Texture();
        
        try
        {
            spriteSheet.loadFromFile(Paths.get("sprites/spritesheet.png"));
            ballTex.loadFromFile(Paths.get("sprites/ball.png"));
        }
        catch(IOException ex)
        {
            System.out.println(ex+" occured while trying to load textures.");
        }
                
        //Set the sprites to their respective textures and configure them
        playerPaddle = new Sprite();
        cpuPaddle = new Sprite();
        ball = new Sprite();
        upperBound = new Sprite();
        lowerBound = new Sprite();
        
        playerPaddle.setTexture(spriteSheet);
        playerPaddle.setTextureRect(new IntRect(0,12,10,75));
        playerPaddle.setOrigin(playerPaddle.getLocalBounds().width*0.5f, playerPaddle.getLocalBounds().height*0.5f);
        playerPaddle.setPosition(10, HEIGHT*0.5f);
        
        cpuPaddle.setTexture(spriteSheet);
        cpuPaddle.setTextureRect(new IntRect(0,12,10,75));
        cpuPaddle.setOrigin(cpuPaddle.getLocalBounds().width*0.5f, cpuPaddle.getLocalBounds().height*0.5f);
        cpuPaddle.setPosition(WIDTH-10, HEIGHT*0.5f);
        
        ball.setTexture(ballTex);
        ball.setOrigin(ball.getLocalBounds().width*0.5f, ball.getLocalBounds().height*0.5f);
        ball.setPosition(WIDTH*0.5f, HEIGHT*0.5f);
        
        upperBound.setTexture(spriteSheet);
        upperBound.setTextureRect(new IntRect(0,0,800,10));
        upperBound.setPosition(0,0);
        
        lowerBound.setTexture(spriteSheet);
        lowerBound.setTextureRect(new IntRect(0,0,800,10));
        lowerBound.setPosition(0, HEIGHT-lowerBound.getGlobalBounds().height);
        
        //Set the mid bound
        mid = new RectangleShape(new Vector2f(1,HEIGHT-2*upperBound.getGlobalBounds().height));
        mid.setOrigin(mid.getGlobalBounds().width*0.5f, mid.getGlobalBounds().height*0.5f);
        mid.setPosition(new Vector2f(WIDTH*0.5f, HEIGHT*0.5f));
        mid.setFillColor(Color.RED);
        
        //Set fonts & text
        font = new Font();
        try
        {
            font.loadFromFile(Paths.get("fonts/RiseStarHand.ttf"));
        }
        catch(IOException ex)
        {
            System.out.println(ex+" occured while trying to load font.");
        }
        
        Text pauseMsg = new Text();
        Text scoreText = new Text();
        Text soundText = new Text();
        
        pauseMsg.setFont(font);
        pauseMsg.setCharacterSize(30);
        pauseMsg.setColor(Color.WHITE);
        
        scoreText.setFont(font);
        scoreText.setCharacterSize(25);
        scoreText.setColor(Color.WHITE);
        
        soundText.setFont(font);
        soundText.setCharacterSize(40);
        soundText.setColor(Color.WHITE);
        
        //Load the sounds in their respective SoundBuffer and set the sounds
        hitBuff = new SoundBuffer();
        menuBuff = new SoundBuffer();
        winBuff = new SoundBuffer();
        
        try
        {
            hitBuff.loadFromFile(Paths.get("sounds/hit.wav"));
            menuBuff.loadFromFile(Paths.get("sounds/menu.wav"));
            winBuff.loadFromFile(Paths.get("sounds/win.wav"));
        }
        catch(IOException ex)
        {
            System.out.println(ex+" occured while loading sounds.");
        }
        
        hitSound = new Sound(hitBuff);
        menuSound = new Sound(menuBuff);
        winSound = new Sound(winBuff);
        
        boolean playSound = true;
        boolean showSoundMsg = false;
        
        String soundONMsg = "Sound turned ON";
        String soundOFFMsg = "Sound turned OFF";
        soundText.setString(soundOFFMsg);
        soundText.setOrigin(soundText.getGlobalBounds().width*0.5f, soundText.getGlobalBounds().height*0.5f);
        soundText.setPosition(WIDTH*0.5f, HEIGHT*0.5f);
        
        Clock soundMsgClock = new Clock();
        
        //Score variables
        int playerScore = 0;
        int cpuScore = 0;
        String scoreStr = "You: "+playerScore+" CPU: "+cpuScore;
        scoreText.setString(scoreStr);
        scoreText.setPosition(0, upperBound.getGlobalBounds().height+2);
        
        //Set the framerate variables
        Clock clock = new Clock();
        float delta;
        
        //Set the  movement variables
        rand = new Random();
        int PADSPEED = 250;
        int BALLSPEED = 350;
        float xWeight = (rand.nextInt(10)+10)/20.0f;
        float yWeight = (rand.nextInt(10)+10)/20.0f;
        int xDir = 1;
        int yDir = 1;
        
        //Menu variables
        boolean isPaused = true;
        String pauseStr = "Controls:-\nUp: UP Key\nDown: DOWN Key\nPause: P Key\nSound ON/OFF: S\nPress SPACE to start!";
        pauseMsg.setString(pauseStr);
        pauseMsg.setOrigin(pauseMsg.getGlobalBounds().width*0.5f, pauseMsg.getGlobalBounds().height*0.5f);
        pauseMsg.setPosition(WIDTH*0.5f, HEIGHT*0.5f);
        
        while(window.isOpen())
        {
            delta = clock.restart().asSeconds();
            //If game is paused
            if(isPaused)
            {
                //Handle events
                for(Event e: window.pollEvents())
                {
                    switch(e.type)
                    {
                        case CLOSED:
                            window.close();
                            break;
                        case KEY_RELEASED:
                            if(e.asKeyEvent().key == Keyboard.Key.SPACE)
                                isPaused = false;
                            break;
                    }
                }
                
                //Draw stuff to screen
                window.clear(Color.BLACK);
                window.draw(ball);
                window.draw(playerPaddle);
                window.draw(cpuPaddle);
                window.draw(upperBound);
                window.draw(lowerBound);
                window.draw(pauseMsg);
                window.display();
                
                //Set text back to pause msg if user presses space
                if(!isPaused)
                {
                    pauseStr = "\t  PAUSED\nPress SPACE to play!";
                    pauseMsg.setString(pauseStr);
                    pauseMsg.setOrigin(pauseMsg.getGlobalBounds().width*0.5f, pauseMsg.getGlobalBounds().height*0.5f);
                    pauseMsg.setPosition(WIDTH*0.5f, HEIGHT*0.5f);
                    if(playSound)
                        menuSound.play();
                }
            }
            
            //If game isn't paused
            else if(!isPaused)
            {
                //Handle events
                for(Event e: window.pollEvents())
                {
                    switch(e.type)
                    {
                        case CLOSED:
                            window.close();
                            break;
                        case KEY_RELEASED:
                            if(e.asKeyEvent().key == Keyboard.Key.P)
                            {
                                isPaused = true;
                                if(playSound)
                                    menuSound.play();
                            
                            }
                            else if(e.asKeyEvent().key == Keyboard.Key.S)
                            {
                                if(playSound)
                                {
                                    playSound = false;
                                    soundText.setString(soundOFFMsg);
                                }
                                else if(!playSound)
                                {
                                    playSound = true;
                                    soundText.setString(soundONMsg);
                                }
                                showSoundMsg = true;
                                soundMsgClock.restart();
                            }
                            break;
                    }
                }
                
                //Some frequently used function calls returning values
                //XX = X Coordinate -- YY = Y Coordinate -- HT = Height
                float cpuYY = cpuPaddle.getPosition().y;
                float playerYY = playerPaddle.getPosition().y;
                float ballXX = ball.getPosition().x;
                float ballYY = ball.getPosition().y;
                float playerHT = playerPaddle.getLocalBounds().height;
                float cpuHT = playerHT;
                float lowbYY = lowerBound.getPosition().y;
                
                //Player movement
                float playerPosY = playerYY+playerHT*0.5f;
                
                if(Keyboard.isKeyPressed(Keyboard.Key.UP) && playerPosY > 87)
                    playerPaddle.move(0, -PADSPEED*delta);
                else if(Keyboard.isKeyPressed(Keyboard.Key.DOWN) && playerPosY < lowbYY)
                    playerPaddle.move(0, PADSPEED*delta);
                
                //CPU movement
                float cpuPosY = cpuYY+cpuHT*0.5f;
                
                if(cpuPosY > 87 && cpuYY > ballYY)
                    cpuPaddle.move(0, -PADSPEED*delta);
                else if(cpuPosY < lowbYY && cpuYY < ballYY)
                    cpuPaddle.move(0, PADSPEED*delta);
                
                //Check if ball collides with up/low bounds and bounce back if it does
                if(isColliding(ball, upperBound))
                {
                    yDir = 1;
                    if(playSound)
                        hitSound.play();
                }
                else if(isColliding(ball, lowerBound))
                {
                    yDir = -1;
                    if (playSound)
                        hitSound.play();
                }
                
                //Check if ball collides with any one paddle
                if(isColliding(ball, cpuPaddle))
                {
                    xDir = -1;
                    if(ballYY < cpuYY)
                        yDir = -1;
                    else
                        yDir = 1;
                    
                    xWeight = (rand.nextInt(10)+10)/20.0f;
                    yWeight = (rand.nextInt(10)+10)/20.0f;
                    if(playSound)
                        hitSound.play();
                }
                else if(isColliding(ball, playerPaddle))
                {
                    xDir = 1;
                    if(ballYY < playerYY)
                        yDir = -1;
                    else
                        yDir = 1;
                    
                    xWeight = (rand.nextInt(10)+10)/20.0f;
                    yWeight = (rand.nextInt(10)+10)/20.0f;
                    if(playSound)
                        hitSound.play();
                }
                
                //Move the ball
                ball.move(xDir*BALLSPEED*xWeight*delta, yDir*BALLSPEED*yWeight*delta);
                
                //Check win/loss
                if(ballXX < 0)
                {
                    cpuScore++;
                    xWeight = 0.5f;
                    yWeight = 0.5f;
                    isPaused = true;
                    pauseStr = "\t+1 for CPU!\nPress SPACE to play!";
                    pauseMsg.setString(pauseStr);
                    pauseMsg.setOrigin(pauseMsg.getGlobalBounds().width*0.5f, pauseMsg.getGlobalBounds().height*0.5f);
                    pauseMsg.setPosition(WIDTH*0.5f, HEIGHT*0.5f);
                    ball.setPosition(WIDTH*0.5f, HEIGHT*0.5f);
                    scoreStr = "You: "+playerScore+" CPU: "+cpuScore;
                    scoreText.setString(scoreStr);
                    scoreText.setPosition(0, upperBound.getGlobalBounds().height+2);
                    if(playSound)
                        winSound.play();
                }
                else if(ballXX > WIDTH)
                {
                    playerScore++;
                    xWeight = 0.5f;
                    yWeight = 0.5f;
                    isPaused = true;
                    pauseStr = "\t+1 for you!\nPress SPACE to play!";
                    pauseMsg.setString(pauseStr);
                    pauseMsg.setOrigin(pauseMsg.getGlobalBounds().width*0.5f, pauseMsg.getGlobalBounds().height*0.5f);
                    pauseMsg.setPosition(WIDTH*0.5f, HEIGHT*0.5f);
                    ball.setPosition(WIDTH*0.5f, HEIGHT*0.5f);
                    scoreStr = "You: "+playerScore+" CPU: "+cpuScore;
                    scoreText.setString(scoreStr);
                    scoreText.setPosition(0, upperBound.getGlobalBounds().height+2);
                    if(playSound)
                        winSound.play();
                }
                
                //Draw stuff to screen
                window.clear(Color.BLACK);
                window.draw(ball);
                window.draw(playerPaddle);
                window.draw(cpuPaddle);
                window.draw(mid);
                window.draw(upperBound);
                window.draw(lowerBound);
                window.draw(scoreText);
                if(showSoundMsg && soundMsgClock.getElapsedTime().asSeconds() <= 2)
                {
                    window.draw(soundText);
                    if(soundMsgClock.getElapsedTime().asSeconds() > 2)
                        showSoundMsg = false;
                }
                window.display();
            }
        }
        
    }
    
    //Collision detection function
    public static boolean isColliding(Sprite a, Sprite b)
    {
        return a.getGlobalBounds().intersection(b.getGlobalBounds()) != null;
    }
}